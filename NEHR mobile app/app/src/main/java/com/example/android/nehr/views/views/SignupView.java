package com.example.android.nehr.views.views;


import com.example.android.nehr.models.SignupModel;
import com.fasterxml.jackson.databind.ser.Serializers;

/**
 * Created by omar on 11/28/2017.
 */

public interface SignupView extends BaseView {
    void signupFailed() ;
    void signupSuccess(SignupModel signupModel) ;
}

package com.example.android.nehr.views.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.android.nehr.R;
import com.example.android.nehr.sessions.SaveSharedPreference;

public class startActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        if(SaveSharedPreference.getMobile(getApplicationContext()).length() == 0)
        {
            Intent i = new Intent(startActivity.this, loginActivity.class);
            startActivity(i);
            finish();
        }
        else
        {
            Intent m = new Intent(startActivity.this,dashboardActivity.class);
            startActivity(m);
            finish();
        }
    }
}

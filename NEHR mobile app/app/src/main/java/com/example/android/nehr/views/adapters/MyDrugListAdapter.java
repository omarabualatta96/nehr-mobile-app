package com.example.android.nehr.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.nehr.R;
import com.example.android.nehr.models.DrugHistory;

import java.util.List;

/**
 * Created by omar on 2/10/2018.
 */

public class MyDrugListAdapter extends ArrayAdapter<DrugHistory> {
    public MyDrugListAdapter(Context context, List<DrugHistory> drugs) {
        super(context, 0, drugs);
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DrugHistory drugHistory = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.my_drug_list, parent, false);
        }
        // Lookup view for data population
        TextView drugName = (TextView) convertView.findViewById(R.id.myDrugNameTxt);
        // Populate the data into the template view using the data object
        drugName.setText(drugHistory.getName());
        // Return the completed view to render on screen
        return convertView;
    }
}

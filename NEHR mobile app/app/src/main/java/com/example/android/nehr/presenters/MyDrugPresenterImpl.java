package com.example.android.nehr.presenters;

import android.content.Context;

import com.example.android.nehr.interactors.DrugInteractor;
import com.example.android.nehr.interactors.DrugInteractorImpl;
import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.views.views.DrugSearchView;
import com.example.android.nehr.views.views.MyDrugsView;

import java.util.List;

/**
 * Created by omar on 2/10/2018.
 */

public class MyDrugPresenterImpl implements MyDrugPresenter {

    MyDrugsView view ;
    DrugInteractor interactor;
    SaveSharedPreference sharedPreference ;

    public MyDrugPresenterImpl(MyDrugsView view) {
        this.view = view;
        this.interactor=new DrugInteractorImpl() {
        };
    }

    @Override
    public void getMyDrugs() {
        interactor.callGetDrugAPI(this,SaveSharedPreference.getnId(view.getContext()));
    }

    @Override
    public void success(List<DrugHistory> drugHistory) {
        view.showDrugs(drugHistory);
    }

    @Override
    public void fail() {
        view.fail();
    }
}

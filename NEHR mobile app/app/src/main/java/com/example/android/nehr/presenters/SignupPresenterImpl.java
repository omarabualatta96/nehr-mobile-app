package com.example.android.nehr.presenters;


import com.example.android.nehr.models.SignupModel;
import com.example.android.nehr.interactors.SignupInteractor;
import com.example.android.nehr.interactors.SignupInteractorImpl;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.views.views.SignupView;

/**
 * Created by omar on 11/28/2017.
 */

public class SignupPresenterImpl implements SignupPresenter {

    SignupView view;
    SignupInteractor interactor;

    public SignupPresenterImpl(SignupView view) {
        this.view = view;
        this.interactor=new SignupInteractorImpl();
    }

    @Override
    public void attmeptSignup(SignupModel signupModel) {
            interactor.callSignupAPI(this,signupModel);

    }

    @Override
    public void signupSuccess(SignupModel signupModel) {

        SaveSharedPreference.setMobile(view.getContext(),signupModel.getMobileNumber());
        SaveSharedPreference.setFirstName(view.getContext(),signupModel.getFirstName());
        SaveSharedPreference.setLastName(view.getContext(),signupModel.getLastName());
        SaveSharedPreference.setnId(view.getContext(), signupModel.getnId());
        SaveSharedPreference.setEmail(view.getContext(), signupModel.getEmail());
        SaveSharedPreference.setPassword(view.getContext(),signupModel.getPasswordHash());
        SaveSharedPreference.setGender(view.getContext(), signupModel.getGender());
        SaveSharedPreference.setDay(view.getContext(), signupModel.getDateDay());
        SaveSharedPreference.setMonth(view.getContext(), signupModel.getDateMonth());
        SaveSharedPreference.setYear(view.getContext(), signupModel.getDateYear());

        view.signupSuccess(signupModel);

    }

    @Override
    public void signupFailure() {
        view.signupFailed();
    }
}

package com.example.android.nehr.views.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.nehr.R;
import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.presenters.MyDrugPresenter;
import com.example.android.nehr.presenters.MyDrugPresenterImpl;
import com.example.android.nehr.views.adapters.MyDrugListAdapter;
import com.example.android.nehr.views.views.MyDrugsView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class myDrugsActivity extends AppCompatActivity implements MyDrugsView {

    @BindView(R.id.myDrugListView) ListView myDrugListView ;
    @BindView(R.id.addDrugBtn)
    FloatingActionButton addDrugBtn ;

    DrugHistory drugHistories = new DrugHistory();
    MyDrugPresenter presenter = new MyDrugPresenterImpl(this) ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mydrugs);
        ButterKnife.bind(this);
        presenter.getMyDrugs();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            this.startActivity(new Intent(this,dashboardActivity.class));
        }
        return true;
    }

    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }

    @Override
    public void showDrugs(List<DrugHistory> drugHistory) {
        MyDrugListAdapter adapter = new MyDrugListAdapter(this,drugHistory);
        myDrugListView.setAdapter(adapter);
        myDrugListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(),myDrugHistoryDetails.class);
                drugHistories.setName(adapter.getItem(position).getName());
                drugHistories.setStartDate(adapter.getItem(position).getStartDate());
                drugHistories.setEndDate(adapter.getItem(position).getEndDate());
                intent.putExtra("drugHistory",drugHistories);
                startActivity(intent);
            }
        });

    }
    @OnClick(R.id.addDrugBtn)
    public void addDrugBtnClicked(){
        Intent i = new Intent(myDrugsActivity.this, alldrugsActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void fail() {
        Toast.makeText(myDrugsActivity.this,"fail",Toast.LENGTH_SHORT).show();
    }
}

package com.example.android.nehr.Services;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by mahmoud on 01/07/18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService{
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String message = remoteMessage.getNotification().getBody().toString();
        Map<String, String> data = remoteMessage.getData();

        Intent i = new Intent(this,com.example.android.nehr.views.activities.messageReciver.class);
        i.putExtra("MESSAGE",data.get("msg"));
        i.putExtra("doctorName", data.get("doctorName"));
        i.putExtra("doctorEmail", data.get("doctorEmail"));


        Log.d(TAG, "Message Received: "+remoteMessage.getNotification().getBody().toString());
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);


    }
}

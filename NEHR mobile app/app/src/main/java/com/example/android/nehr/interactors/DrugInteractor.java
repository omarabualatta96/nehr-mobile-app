package com.example.android.nehr.interactors;

import com.example.android.nehr.models.DrugHistory;
import com.example.android.nehr.models.DrugModel;
import com.example.android.nehr.models.PatientDrugHistory;
import com.example.android.nehr.presenters.DrugDetailsPresenter;
import com.example.android.nehr.presenters.DrugSearchPresenter;
import com.example.android.nehr.presenters.MyDrugHistoryDetailsPresenter;
import com.example.android.nehr.presenters.MyDrugPresenter;

import java.util.List;

/**
 * Created by omar on 2/4/2018.
 */

public interface DrugInteractor {
    void callSearchAPI(DrugSearchPresenter presenter , String name);
    void callAddDrugAPI(DrugDetailsPresenter presenter , PatientDrugHistory patientDrugHistory);
    void callAddDrugHistoryAPI(MyDrugHistoryDetailsPresenter presenter , PatientDrugHistory patientDrugHistory);
    void callGetDrugAPI(MyDrugPresenter presenter, String id);

}

package com.example.android.nehr.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by mahmoud on 2/10/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class PatientDrugHistory {

    private String nID;
    private List<DrugHistory> drugs;




    public String getnId() {
        return nID;
    }

    public void setnId(String nID) {
        this.nID = nID;
    }

    public List<DrugHistory> getDrugs() {
        return drugs;
    }

    public void setDrugs(List<DrugHistory> drugs) {
        this.drugs = drugs;
    }


}

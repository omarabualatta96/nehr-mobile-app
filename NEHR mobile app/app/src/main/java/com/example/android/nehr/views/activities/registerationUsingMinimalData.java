package com.example.android.nehr.views.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.nehr.R;
import com.example.android.nehr.models.SignupModel;
import com.example.android.nehr.presenters.SignupPresenter;
import com.example.android.nehr.presenters.SignupPresenterImpl;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.views.views.SignupView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class registerationUsingMinimalData extends AppCompatActivity implements SignupView {

    @BindView(R.id.mobileNumberTxt) EditText mobileNumberTxt;
    @BindView(R.id.nationalIDTxt) EditText nationalIDTxt;
    @BindView(R.id.passwordTxt) EditText passwordTxt;

    SignupPresenter presenter = new SignupPresenterImpl(this);
    SignupModel signupModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration_using_minimal_data);
        ButterKnife.bind(this);
        signupModel = new SignupModel();
    }

    @OnClick(R.id.registerBtn)
    public void regersterBtnClicked() {
       if(isValid())
        {
            signupModel.setMobileNumber(String.valueOf(mobileNumberTxt.getText()));
            signupModel.setnId(String.valueOf(nationalIDTxt.getText()));
            signupModel.setPasswordHash(String.valueOf(passwordTxt.getText()));
            presenter.attmeptSignup(signupModel);

        }

    }
    @Override
    public void signupFailed() {
        Toast.makeText(registerationUsingMinimalData.this, "signup failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void signupSuccess(SignupModel signupModel) {
        this.navigateToDashboard();

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            this.startActivity(new Intent(registerationUsingMinimalData.this,registerSelection.class));
        }
        return true;
    }

    @Override
    public Context getContext(){
        return this.getApplicationContext();
    }


    private boolean isValid() {
        boolean valid = true;
        if (mobileNumberTxt.getText().toString().equals("")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter your mobile Number", Toast.LENGTH_SHORT).show();
        }

        if (nationalIDTxt.getText().toString().equals("") || nationalIDTxt.length() == 13) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter a valid ID Nuumber", Toast.LENGTH_SHORT).show();
        }

        if (passwordTxt.getText().toString().equals("")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter your Password", Toast.LENGTH_SHORT).show();

        }

        return valid;
    }

    void navigateToDashboard(){
        Toast.makeText(registerationUsingMinimalData.this, "signup success", Toast.LENGTH_LONG).show();
        Intent i = new Intent(registerationUsingMinimalData.this, dashboardActivity.class);
        startActivity(i);
        finish();
    }



}

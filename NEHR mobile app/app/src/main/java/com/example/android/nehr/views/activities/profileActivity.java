package com.example.android.nehr.views.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


import com.example.android.nehr.R;
import com.example.android.nehr.models.SignupModel;
import com.example.android.nehr.presenters.SignupPresenter;
import com.example.android.nehr.presenters.SignupPresenterImpl;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.views.views.SignupView;

public class profileActivity extends AppCompatActivity implements SignupView {

    boolean editEnabled = false ;
    SignupModel signupModel = new SignupModel() ;
    SignupPresenter signupPresenter = new SignupPresenterImpl(this);

    @BindView(R.id.firstNameTxt) EditText firstNameTxt ;
    @BindView(R.id.lastNameTxt) EditText lastNameTxt ;
    @BindView(R.id.mobileNumberTxt) EditText mobileTxt ;
    @BindView(R.id.emailTxt)EditText emailTxt;
    @BindView(R.id.dateDayTxt)EditText dateDayTxt;
    @BindView(R.id.dateMonthTxt)EditText dateMonthTxt;
    @BindView(R.id.dateYearTxt)EditText dateYearTxt;
    @BindView(R.id.genderTxt)EditText genderTxt;
    @BindView(R.id.editBtn) Button editBtn;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        firstNameTxt.setEnabled(false);
        lastNameTxt.setEnabled(false);
        emailTxt.setEnabled(false);
        mobileTxt.setEnabled(false);
        genderTxt.setEnabled(false);
        dateDayTxt.setEnabled(false);
        dateMonthTxt.setEnabled(false);
        dateYearTxt.setEnabled(false);



        firstNameTxt.setText(SaveSharedPreference.getFirstName(getApplicationContext()));
        lastNameTxt.setText(SaveSharedPreference.getLastName(getApplicationContext()));
        emailTxt.setText(SaveSharedPreference.getEmail(getApplicationContext()));
        mobileTxt.setText(SaveSharedPreference.getMobile(getApplicationContext()));
        genderTxt.setText(SaveSharedPreference.getGender(getApplicationContext()));
        dateDayTxt.setText(SaveSharedPreference.getDay(getApplicationContext()));
        dateMonthTxt.setText(SaveSharedPreference.getMonth(getApplicationContext()));
        dateYearTxt.setText(SaveSharedPreference.getYear(getApplicationContext()));





    }
@OnClick(R.id.editBtn)public void edit(){
        editEnabled = !editEnabled;
        if(editEnabled){
            editBtn.setText("Save");
            firstNameTxt.setEnabled(true);
            lastNameTxt.setEnabled(true);
            emailTxt.setEnabled(true);
            mobileTxt.setEnabled(true);
            dateDayTxt.setEnabled(true);
            dateMonthTxt.setEnabled(true);
            dateYearTxt.setEnabled(true);
            genderTxt.setEnabled(true);
        }
        else
        {
            editBtn.setText("Edit");
            firstNameTxt.setEnabled(false);
            lastNameTxt.setEnabled(false);
            emailTxt.setEnabled(false);
            mobileTxt.setEnabled(false);
            dateDayTxt.setEnabled(false);
            dateMonthTxt.setEnabled(false);
            dateYearTxt.setEnabled(false);
            genderTxt.setEnabled(false);

            signupModel.setnId(SaveSharedPreference.getnId(getApplicationContext()));
            signupModel.setFirstName(String.valueOf(firstNameTxt.getText()));
            signupModel.setLastName(String.valueOf(lastNameTxt.getText()));
            signupModel.setMobileNumber(String.valueOf(mobileTxt.getText()));
            signupModel.setGender(String.valueOf(genderTxt.getText()));
            signupModel.setEmail(String.valueOf(emailTxt.getText()));
            signupModel.setDateDay(String.valueOf(dateDayTxt.getText()));
            signupModel.setDateMonth(String.valueOf(dateMonthTxt.getText()));
            signupModel.setDateYear(String.valueOf(dateYearTxt.getText()));
            signupPresenter.attmeptSignup(signupModel);

        }
}


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            this.startActivity(new Intent(this,dashboardActivity.class));
        }
        return true;
    }

    @Override
    public void signupFailed() {
        Toast.makeText(profileActivity.this, "failed", Toast.LENGTH_LONG).show();

    }

    @Override
    public void signupSuccess(SignupModel signupModel) {
        Toast.makeText(profileActivity.this, "edited successfully", Toast.LENGTH_LONG).show();

    }

    @Override
    public Context getContext(){
        return this.getApplicationContext();
    }
}

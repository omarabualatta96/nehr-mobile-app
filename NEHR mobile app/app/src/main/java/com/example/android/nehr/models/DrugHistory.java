package com.example.android.nehr.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by mahmoud on 2/10/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class DrugHistory  implements Serializable {
    private String name;
    private String startDate;
    private String endDate;
    private String image ;

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}

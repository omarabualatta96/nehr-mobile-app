package com.example.android.nehr.models;

import android.graphics.Bitmap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

/**
 * Created by omar on 2/4/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class DrugModel implements Serializable {
    private static final long serialVersionUID = 1L;
    String id;
    String name;
    String description;
    List<String> diseases ;
    List<String> effectiveSubstance;
    List<String> symptoms ;
    boolean active ;
    String image ;

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return this.id + "." + this.name  ;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<String> diseases) {
        this.diseases = diseases;
    }

    public List<String> getEffectiveSubstance() {
        return effectiveSubstance;
    }

    public void setEffectiveSubstance(List<String> effectiveSubstance) {
        this.effectiveSubstance = effectiveSubstance;
    }

    public List<String> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(List<String> symptoms) {
        this.symptoms = symptoms;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}

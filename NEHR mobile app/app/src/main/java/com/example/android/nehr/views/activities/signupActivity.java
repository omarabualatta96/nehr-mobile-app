package com.example.android.nehr.views.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.example.android.nehr.R;
import com.example.android.nehr.models.SignupModel;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.presenters.SignupPresenter;
import com.example.android.nehr.presenters.SignupPresenterImpl;
import com.example.android.nehr.views.views.SignupView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import java.util.Calendar;

public class signupActivity extends AppCompatActivity implements SignupView {

    @BindView(R.id.mobileNumberTxt)
    EditText mobileNumberTxt;
    @BindView(R.id.firstNameTxt)
    EditText firstNameTxt;
    @BindView(R.id.lastNameTxt)
    EditText lastNameTxt;
    @BindView(R.id.nationalIDTxt)
    EditText nationalIDTxt;
    @BindView(R.id.emailTxt)
    EditText emailTxt;
    @BindView(R.id.passwordTxt)
    EditText passwordTxt;
    @BindView(R.id.registerBtn)
    Button registerButton;
    @BindView(R.id.dateTxt)
    TextView dateTxt;
    @BindView(R.id.genderRadioGroup)
    RadioGroup genderRadioGroup;

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    SignupPresenter presenter = new SignupPresenterImpl(this);
    SignupModel signupModel;

    int day;
    int month;
    int year;
    String gender;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        signupModel = new SignupModel();

        dateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(
                        signupActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = day + "/" + month + "/" + year;
                dateTxt.setText(date);
                signupModel.setDateYear(String.valueOf(year));
                signupModel.setDateMonth(String.valueOf(month));
                signupModel.setDateDay(String.valueOf(day));
            }
        };


    }

    @OnClick(R.id.registerBtn)
    public void regersterBtnClicked() {

        if (isValid()) {
            signupModel.setMobileNumber(String.valueOf(mobileNumberTxt.getText()));
            signupModel.setFirstName(String.valueOf(firstNameTxt.getText()));
            signupModel.setLastName(String.valueOf(lastNameTxt.getText()));
            signupModel.setnId(String.valueOf(nationalIDTxt.getText()));
            signupModel.setEmail(String.valueOf(emailTxt.getText()));
            signupModel.setPasswordHash(String.valueOf(passwordTxt.getText()));
            signupModel.setGender(gender);
            presenter.attmeptSignup(signupModel);
        }

    }


    @Override
    public void signupFailed() {
        Toast.makeText(signupActivity.this, "signup failed", Toast.LENGTH_LONG).show();

    }

    @Override
    public void signupSuccess(SignupModel signupModel) {
        Toast.makeText(signupActivity.this, "signup success", Toast.LENGTH_LONG).show();
        naviagateToDashboard();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.startActivity(new Intent(signupActivity.this, registerSelection.class));
        }
        return true;
    }

    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }


    private boolean isValid() {
        boolean valid = true;
        if (mobileNumberTxt.getText().toString().equals("")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter your mobile Number", Toast.LENGTH_SHORT).show();
        }
        if (firstNameTxt.getText().toString().equals("")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter your First Name", Toast.LENGTH_SHORT).show();
        }
        if (lastNameTxt.getText().toString().equals("")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter your Last Name", Toast.LENGTH_SHORT).show();
        }
        if (nationalIDTxt.getText().toString().equals("") || nationalIDTxt.length() == 13) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter a valid ID Nuumber", Toast.LENGTH_SHORT).show();
        }
        if (emailTxt.getText().toString().equals("") || !Patterns.EMAIL_ADDRESS.matcher(emailTxt.getText()).matches()) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter your Email", Toast.LENGTH_SHORT).show();
        }
        if (passwordTxt.getText().toString().equals("")) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Please enter your Password", Toast.LENGTH_SHORT).show();

        }


        int selectedId = genderRadioGroup.getCheckedRadioButtonId();
        if (selectedId == R.id.maleRadioBtn)
            gender = "Male";
        else
            gender = "Female";

        return valid;
    }


    private void naviagateToDashboard() {
        Intent i = new Intent(signupActivity.this, dashboardActivity.class);
        startActivity(i);
        finish();
    }


}

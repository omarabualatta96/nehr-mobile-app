package com.example.android.nehr.views.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.nehr.R;

import com.example.android.nehr.models.PrescriptionModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PrescriptionAdapter extends ArrayAdapter<PrescriptionModel> {


    public PrescriptionAdapter(Context context, List<PrescriptionModel> prescriptionModelsList) {
        super(context, 0, prescriptionModelsList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        PrescriptionModel prescriptionModel = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.my_prescription_list, parent, false);
        }
        // Lookup view for data population
        TextView doctorName = (TextView) convertView.findViewById(R.id.doctorNameTxt);
        TextView presctiptionDate = (TextView) convertView.findViewById(R.id.prescreptionDateTxt);
        ImageView imagePreview = (ImageView) convertView.findViewById(R.id.imagePreview);
        assert prescriptionModel != null;
        Bitmap image = getBitmapFromString(Objects.requireNonNull(prescriptionModel).getPrescriptionImage());

        // Populate the data into the template view using the data object
        doctorName.setText(prescriptionModel.getDoctorName());
        presctiptionDate.setText(prescriptionModel.getPrescriptionDate());
        imagePreview.setImageBitmap(image);


        // Return the completed view to render on screen
        return convertView;
    }


    private Bitmap getBitmapFromString(String jsonString) {
        /*
         * This Function converts the String back to Bitmap
         * */
        byte[] decodedString = Base64.decode(jsonString, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
}
package com.example.android.nehr.views.activities;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.android.nehr.R;
import com.example.android.nehr.alarm.AlarmMainActivity;
import com.example.android.nehr.sessions.SaveSharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class dashboardActivity extends AppCompatActivity   {

    @BindView(R.id.profileCard) CardView profile ;
    @BindView(R.id.myDrugsCard) CardView myDrugs ;
    @BindView(R.id.searchDrugsCard) CardView allDrugs ;
    @BindView(R.id.logoutCard) CardView logout ;
    @BindView(R.id.prescreption) CardView prescription ;
    @BindView(R.id.alarms) CardView alarms;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),profileActivity.class);
                startActivity(intent);
                finish();
            }
        });

        myDrugs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),myDrugsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        allDrugs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),alldrugsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SaveSharedPreference.logout(getApplicationContext());
                Intent i = new Intent(dashboardActivity.this, startActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    @OnClick(R.id.alarms)
    public void startAlarmLayout(){
        Intent i = new Intent(dashboardActivity.this,AlarmMainActivity.class);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.prescreption)
        public void startPrescriptionLayout(){
            Intent i = new Intent(dashboardActivity.this, myPrescriptions.class);
            startActivity(i);
            finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profile:
                Intent intent = new Intent(getApplicationContext(),profileActivity.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.myDrugs:
                Intent intent1 = new Intent(getApplicationContext(),myDrugsActivity.class);
                startActivity(intent1);
                finish();
                return true;
            case R.id.allDrugs:
                Intent intent2 = new Intent(getApplicationContext(),alldrugsActivity.class);
                startActivity(intent2);
                finish();
                return true;
            case R.id.logout:
                SaveSharedPreference.logout(getApplicationContext());
                Intent i = new Intent(dashboardActivity.this, startActivity.class);
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            finishAffinity();
        }
        return true;
    }


}

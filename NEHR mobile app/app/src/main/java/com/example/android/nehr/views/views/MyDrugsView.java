package com.example.android.nehr.views.views;

import com.example.android.nehr.models.DrugHistory;

import java.util.List;

/**
 * Created by omar on 2/10/2018.
 */

public interface MyDrugsView extends BaseView {
    void showDrugs(List<DrugHistory> drugHistory);
    void fail();
}

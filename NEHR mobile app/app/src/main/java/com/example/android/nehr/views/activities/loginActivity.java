package com.example.android.nehr.views.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.android.nehr.R;
import com.example.android.nehr.presenters.LoginPresenter;
import com.example.android.nehr.presenters.LoginPresenterImpl;
import com.example.android.nehr.views.views.LoginView;
import com.example.android.nehr.models.LoginModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class loginActivity extends AppCompatActivity implements LoginView {


    @BindView(R.id.login_phoneNumber)
    EditText mobileNumber;
    @BindView(R.id.login_password)
    EditText password;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.signup_btn)
    TextView signupBtn;


    LoginModel loginModel;
    LoginPresenter loginPresenter = new LoginPresenterImpl(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loginModel = new LoginModel();

    }

    @OnClick(R.id.login_btn)
    public void loginBtnClicked() {
        if (!isValid()) {
            loginFailed();
        } else {
            loginModel.setMobileNumber(String.valueOf(mobileNumber.getText()));
            loginModel.setPasswordHash(String.valueOf(password.getText()));
            loginPresenter.attmeptLogin(loginModel);
        }

    }

    @OnClick(R.id.signup_btn)
    public void signupBtnClicked() {
        navigateToSignup();
    }

    @Override
    public void loginFailed() {
        Toast.makeText(loginActivity.this, "login failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void loginSuccess(LoginModel loginModel) {
        Toast.makeText(loginActivity.this, "login success", Toast.LENGTH_LONG).show();
        navigateToDashboard();

    }


    public boolean isValid() {
        boolean valid = true;

        String phone = mobileNumber.getText().toString();
        String pass = password.getText().toString();

        if (phone.isEmpty() || !Patterns.PHONE.matcher(phone).matches()) {
            mobileNumber.setError("enter a valid phone number");
            valid = false;
        } else {
            mobileNumber.setError(null);
        }

        if (pass.isEmpty() || password.length() < 8) {
            password.setError("at least 8 characters");
            valid = false;
        } else {
            password.setError(null);
        }

        return valid;
    }

    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }

    private void navigateToDashboard() {
        Intent i = new Intent(loginActivity.this, dashboardActivity.class);
        startActivity(i);
        finish();
    }

    private void navigateToSignup() {
        Intent intent = new Intent(getApplicationContext(), registerSelection.class);
        startActivity(intent);
        finish();
    }


}

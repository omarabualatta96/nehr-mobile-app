package com.example.android.nehr.views.activities;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.nehr.R;
import com.example.android.nehr.sessions.SaveSharedPreference;
//import com.example.android.nehr.views.alarm.AlarmNotificationService;
//import com.example.android.nehr.views.alarm.AlarmSoundService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class myAlarms extends AppCompatActivity {

    @BindView(R.id.adherenceTxt)TextView adherenceTxt ;
    String adherenceNumberString ;
    int adherenceNumber ;
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_alarms);
        ButterKnife.bind(this);
        if(SaveSharedPreference.getAdherence(getApplicationContext()).length()!= 0){
            adherenceNumberString = SaveSharedPreference.getAdherence(getApplicationContext());
            adherenceNumber = Integer.parseInt(adherenceNumberString);
        }
        AlertDialog.Builder alert = new AlertDialog.Builder(myAlarms.this);
        alert.setMessage("Did you take your medication ? ").setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        adherenceNumber+=1;
                        adherenceTxt.setText(String.valueOf(adherenceNumber));
                        adherenceTxt.append("%");
                        SaveSharedPreference.setAdherence(getApplicationContext(),String.valueOf(adherenceNumber));
                       // stopAlarm();
                        dialogInterface.cancel();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        adherenceNumber-=1 ;
                        adherenceTxt.setText(String.valueOf(adherenceNumber));
                        adherenceTxt.append("%");
                        SaveSharedPreference.setAdherence(getApplicationContext(),String.valueOf(adherenceNumber));
                        //stopAlarm();
                       dialogInterface.cancel();
                    }
                });
            AlertDialog alertDialog = alert.create();
            alertDialog.setTitle("medication alarm");
            alertDialog.show();

    }

  /*  public void stopAlarm(){
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);//cancel the alarm manager of the pending intent


        //Stop the Media Player Service to stop sound
        stopService(new Intent(myAlarms.this, AlarmSoundService.class));

        //remove the notification from notification tray
        NotificationManager notificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(AlarmNotificationService.NOTIFICATION_ID);

        Toast.makeText(this, "Alarm Canceled/Stop by User.", Toast.LENGTH_SHORT).show();
    }*/

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.startActivity(new Intent(this, dashboardActivity.class));
        }
        return true;
    }
}

package com.example.android.nehr.presenters;

import com.example.android.nehr.interactors.CallAuthorizationDoctorAPI;
import com.example.android.nehr.interactors.CallAuthorizationDoctorAPIImpl;
import com.example.android.nehr.views.views.DoctorPermissionView;

public class DoctorPermissionPresenterImpl implements DoctorPermissionPresenter {

    DoctorPermissionView view ;
    CallAuthorizationDoctorAPI interactor;

    public DoctorPermissionPresenterImpl(DoctorPermissionView view){
        this.view=view;
        interactor = new CallAuthorizationDoctorAPIImpl();
    }
    @Override
    public void sendAuthorizationResponse(String response, String doctorEmail, String patientNID) {
        interactor.callLoginAPI(response,doctorEmail,patientNID);
    }
}

package com.example.android.nehr.presenters;


import com.example.android.nehr.interactors.LoginInteractor;
import com.example.android.nehr.interactors.LoginInteractorImpl;
import com.example.android.nehr.models.LoginModel;
import com.example.android.nehr.sessions.SaveSharedPreference;
import com.example.android.nehr.views.views.LoginView;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by omar on 1/30/2018.
 */

public class LoginPresenterImpl implements LoginPresenter {

    LoginView view;
    LoginInteractor interactor;

    public LoginPresenterImpl(LoginView view) {
        this.view = view;
        this.interactor=new LoginInteractorImpl() {
        };
    }

    @Override
    public void attmeptLogin(LoginModel loginModel) {
        interactor.callLoginAPI(this, loginModel);
    }

    @Override
    public void loginSuccess(LoginModel loginModel) {
        SaveSharedPreference.setMobile(view.getContext(),loginModel.getMobileNumber());
        SaveSharedPreference.setFirstName(view.getContext(),loginModel.getFirstName());
        SaveSharedPreference.setLastName(view.getContext(),loginModel.getLastName());
        SaveSharedPreference.setnId(view.getContext(),loginModel.getnID());
        SaveSharedPreference.setGender(view.getContext(),loginModel.getGender());
        SaveSharedPreference.setEmail(view.getContext(),loginModel.getEmail());
        SaveSharedPreference.setDay(view.getContext(),loginModel.getDateDay());
        SaveSharedPreference.setMonth(view.getContext(),loginModel.getDateMonth());
        SaveSharedPreference.setYear(view.getContext(),loginModel.getDateYear());
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        view.loginSuccess(loginModel);
    }

    @Override
    public void loginFailure() {
        view.loginFailed();
    }
}

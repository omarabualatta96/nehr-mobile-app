package com.example.android.nehr.views.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.nehr.R;
import com.example.android.nehr.Services.DownloadImageService;
import com.example.android.nehr.models.DrugModel;
import com.example.android.nehr.presenters.DrugDetailsPresenter;
import com.example.android.nehr.presenters.DrugDetailsPresenterImpl;
import com.example.android.nehr.views.views.DrugDetailsView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class drugDetails extends AppCompatActivity implements DrugDetailsView {

    @BindView(R.id.name)
    TextView drugName;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.diseases)
    TextView diseases;
    @BindView(R.id.effectiveSubstance)
    TextView effectiveSubstance;
    @BindView(R.id.symptoms)
    TextView symptoms;
    @BindView(R.id.imageView)
    ImageView imageView;

    DrugDetailsPresenter presenter;
    String diseasesText = "";
    String effectiveSubstanceText = "";
    String symptomsText = "";
    String image ="" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drug_details);
        ButterKnife.bind(this);
        this.showDrug(this.getDrugModelFromIntent(this.getIntent()));
        image = this.getDrugModelFromIntent(this.getIntent()).getImage() ;
        presenter = new DrugDetailsPresenterImpl(this);
        new DownloadImageService(imageView).execute(image);

    }


    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }

    private DrugModel getDrugModelFromIntent(Intent intent) {
        return (DrugModel) intent.getSerializableExtra("drugModel");
    }

    @Override
    public void drugAddedSuccess() {
        Toast.makeText(this.getApplicationContext(), "drug added to your drugs", Toast.LENGTH_LONG).show();

    }

    @Override
    public void drugAddFailed() {
        Toast.makeText(this.getApplicationContext(), "can't add this drug to your drugs", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.addDrugBtn)
    public void addToMyDrugs() {
        AlertDialog.Builder alert = new AlertDialog.Builder(drugDetails.this);
        alert.setMessage("Are you sure you want to add this drug to your drugs? ").setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        addDrug();
                        dialogInterface.cancel();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        AlertDialog alertDialog = alert.create();
        alertDialog.setTitle("Add drug");
        alertDialog.show();
    }


    public void addDrug() {
        presenter.addToMyDrugs(this.getDrugModelFromIntent(this.getIntent()));

    }

    public void showDrug(DrugModel drugModel) {
        List<String> diseasesList = new ArrayList<>();
        List<String> effectiveList = new ArrayList<>();
        List<String> symptomsList = new ArrayList<>();

        drugName.setText(drugModel.getName());
        if (drugModel.getDescription() == null) {
            description.setText("not available");
        } else if (drugModel.getDescription().equals("null")) {
            description.setText("not available");
        } else {
            description.setText(drugModel.getDescription());
        }

        // Diseases
        if (drugModel.getDiseases() == null) {
            diseasesList.clear();
            diseasesList.add("not available");
            diseases.setText("not available");
        } else {
            diseasesList = drugModel.getDiseases();
            for (int i = 0; i < diseasesList.size(); i++) {
                diseasesText = diseasesText + diseasesList.get(i) + ", ";
            }
            diseases.setText(diseasesText.substring(0, diseasesText.length() - 2));
        }


        //Effective Substance
        if (drugModel.getEffectiveSubstance() == null) {
            effectiveList.clear();
            effectiveList.add("not available");
            effectiveSubstance.setText("not available");
        } else {
            effectiveList = drugModel.getEffectiveSubstance();
            for (int i = 0; i < effectiveList.size(); i++) {
                effectiveSubstanceText = effectiveSubstanceText + effectiveList.get(i) + ", ";
            }
            effectiveSubstance.setText(effectiveSubstanceText.substring(0, effectiveSubstanceText.length() - 2));
        }

        //Symptoms
        if (drugModel.getSymptoms() == null) {
            symptomsList.clear();
            symptomsList.add("not available");
            symptoms.setText("not available");
        } else {
            symptomsList = drugModel.getSymptoms();
            for (int i = 0; i < symptomsList.size(); i++) {
                symptomsText = symptomsText + symptomsList.get(i) + ", ";
            }
            symptoms.setText(symptomsText.substring(0, symptomsText.length() - 2));
        }


    }

}

package com.example.android.nehr.views.views;

import android.content.Context;

/**
 * Created by mahmoud on 2/6/2018.
 */

public interface BaseView {
    Context getContext();

}

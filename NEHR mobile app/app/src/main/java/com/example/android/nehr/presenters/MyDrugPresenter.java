package com.example.android.nehr.presenters;

import com.example.android.nehr.models.DrugHistory;

import java.util.List;

/**
 * Created by omar on 2/10/2018.
 */

public interface MyDrugPresenter {
    public void getMyDrugs();
    public void success(List<DrugHistory> drugHistory);
    public void fail();
}

package com.example.android.nehr.interactors;

import com.example.android.nehr.models.LoginModel;
import com.example.android.nehr.presenters.LoginPresenter;

public interface CallAuthorizationDoctorAPI {
    void callLoginAPI(String response, String doctorEmail,String patientNID);

}

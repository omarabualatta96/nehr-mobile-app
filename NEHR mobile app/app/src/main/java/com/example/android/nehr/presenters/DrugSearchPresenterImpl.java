package com.example.android.nehr.presenters;

import android.util.Log;

import com.example.android.nehr.interactors.DrugInteractor;
import com.example.android.nehr.interactors.DrugInteractorImpl;
import com.example.android.nehr.models.DrugModel;
import com.example.android.nehr.views.views.DrugSearchView;

import java.util.List;

/**
 * Created by omar on 2/4/2018.
 */



public class DrugSearchPresenterImpl implements DrugSearchPresenter {



    DrugSearchView view;
    DrugInteractor interactor;

    public DrugSearchPresenterImpl(DrugSearchView view) {
        this.view = view;
        this.interactor=new DrugInteractorImpl() {
        };
    }
    @Override
    public void attemptSearch(String drugName) {
        Log.d("ourMsg", "attemptSearchIsCalled");
        interactor.callSearchAPI(this, drugName);
    }

    @Override
    public void searchSuccess(List<DrugModel> drugModel) {
        view.showDrugs(drugModel);
    }

    @Override
    public void searchFailure() {

    }
}

package com.example.android.nehr.presenters;

import com.example.android.nehr.models.DrugModel;

import java.util.List;

/**
 * Created by omar on 2/4/2018.
 */

public interface DrugSearchPresenter {
    void attemptSearch(String drugName);
    void searchSuccess(List<DrugModel> drugModel);
    void searchFailure();


}

package com.example.android.nehr.views.views;

import com.example.android.nehr.models.DrugModel;

import java.util.List;

/**
 * Created by omar on 2/4/2018.
 */

public interface DrugSearchView extends BaseView {
    void showDrugs(List<DrugModel> drugs);
}

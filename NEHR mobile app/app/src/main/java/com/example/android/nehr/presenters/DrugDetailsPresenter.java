package com.example.android.nehr.presenters;

import com.example.android.nehr.models.DrugModel;

/**
 * Created by mahmoud on 2/10/2018.
 */

public interface DrugDetailsPresenter {
    public void addToMyDrugs(DrugModel drug);
    public void drugAddedSuccesfully();
    public void drugAddingFailed();

}

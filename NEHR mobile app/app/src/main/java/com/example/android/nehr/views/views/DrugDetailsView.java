package com.example.android.nehr.views.views;

import com.example.android.nehr.models.DrugModel;

/**
 * Created by mahmoud on 2/10/2018.
 */

public interface DrugDetailsView extends BaseView {
    public void drugAddedSuccess();
    public void drugAddFailed();
}

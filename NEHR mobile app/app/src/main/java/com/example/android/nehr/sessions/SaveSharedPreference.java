package com.example.android.nehr.sessions;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by omar on 12/13/2017.
 */

public class SaveSharedPreference {

    private static final String FIRST_NAME= "firstName";
    private static final String LAST_NAME= "lastName";
    private static final String NID = "nationalID";
    private static final String EMAIL= "email";
    private static final String MOBILE= "mobile";
    private static final String PASSWORD= "password";
    private static final String GENDER = "gender";
    private static final String DAY = "day";
    private static final String MONTH = "month";
    private static final String YEAR = "year";
    private static final String PRES_LIST = "prescriptionsList";
    private static final String ADHERENCE = "adherence";



    private static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setFirstName(Context ctx, String userName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(FIRST_NAME, userName);
        editor.apply();
    }

    public static String getFirstName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(FIRST_NAME, "");
    }
    public static void setLastName(Context ctx, String lastName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(LAST_NAME, lastName);
        editor.apply();
    }

    public static String getLastName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(LAST_NAME, "");
    }

    public static void setnId(Context ctx, String nId)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(NID, nId);
        editor.apply();
    }

    public static String getnId(Context ctx)
    {
        return getSharedPreferences(ctx).getString(NID, "");
    }

    public static void setEmail(Context ctx, String email)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public static String getEmail(Context ctx)
    {
        return getSharedPreferences(ctx).getString(EMAIL, "");
    }

    public static void setMobile(Context ctx, String mobile)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(MOBILE, mobile);
        editor.apply();
    }

    public static String getMobile(Context ctx)
    {
        return getSharedPreferences(ctx).getString(MOBILE, "");
    }

    public static void setPassword(Context ctx, String password)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PASSWORD, password);
        editor.apply();
    }

    public static String getPassword(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PASSWORD, "");
    }

    public static void setGender(Context ctx, String gender)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(GENDER, gender);
        editor.apply();
    }

    public static String getGender(Context ctx)
    {
        return getSharedPreferences(ctx).getString(GENDER, "");
    }

    public static void setDay(Context ctx, String day)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(DAY, day);
        editor.apply();
    }

    public static String getDay(Context ctx)
    {
        return getSharedPreferences(ctx).getString(DAY, "");
    }

    public static void setMonth(Context ctx, String month)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(MONTH, String.valueOf(month));
        editor.apply();
    }

    public static String getMonth(Context ctx)
    {
        return getSharedPreferences(ctx).getString(MONTH, "");
    }

    public static void setYear(Context ctx, String year)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(YEAR, String.valueOf(year));
        editor.apply();
    }

    public static String getYear(Context ctx)
    {
        return getSharedPreferences(ctx).getString(YEAR, "");
    }

    public static void logout(Context ctx)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.clear(); //clear all stored data
        editor.apply();
    }
    public static void setPresList(Context ctx, String prescriptionListJson)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PRES_LIST, prescriptionListJson);
        editor.apply();
    }

    public static String getPresList(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PRES_LIST, "");
    }


    public static void setAdherence(Context ctx, String adherenceNumberString)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(ADHERENCE, adherenceNumberString);
        editor.apply();
    }

    public static String getAdherence(Context ctx)
    {
        return getSharedPreferences(ctx).getString(ADHERENCE, "");
    }
}

package com.example.android.nehr.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by mahmoud on 18/06/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class MobileInstance {

    String mobileInstanceIdToken;
    String nID;



    public String getnId() {
        return nID;
    }

    public void setnId(String nID) {
        this.nID = nID;
    }



    public String getMobileInstanceIdToken(){
        return mobileInstanceIdToken;
    }

    public void setMobileInstanceIdToken(String mobileInstanceIdToken) {
        this.mobileInstanceIdToken = mobileInstanceIdToken;
    }

}
